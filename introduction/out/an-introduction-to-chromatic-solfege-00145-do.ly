\version "2.18.2"
\include "aaron.ly"
\include "chromatic-template.ly"
\include "lilypond-book-preamble.ly"
\language "aaron"

	\makescore 
  \relative do' {
	do' sol' re' la mi'  \bar "|"  ti, fi' di' si ri'  \bar "|"  li, ma' ta' fai dai'  \bar "|"  la, mi' ti' fi di'  \bar "|" \break  si, ri' li' ma ta'  \bar "|"  sol, re' la' mi ti'  \bar "|"  fi, di' si' ri li'  \bar "|"  fa, do' sol' re la'  \bar "|" \break  mi, ti' fi' di si'  \bar "|"  ri, li' ma' ta fai'  \bar "|"  re, la' mi' ti fi'  \bar "|"  di, si' ri' li ma'  \bar "|" \break  do1
  }
  #'(( festival-tempo . 150 )( festival-voice . voice_us2_mbrola )( do-compile . #t )( do-play . #f ))

\paper  {
	#(set! paper-alist (cons '("a4insides" . (cons (* 8 in) (* 3 in))) paper-alist))
	#(set-paper-size "a4insides")
}

