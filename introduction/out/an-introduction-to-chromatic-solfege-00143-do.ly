\version "2.18.2"
\include "aaron.ly"
\include "chromatic-template.ly"
\include "lilypond-book-preamble.ly"
\language "aaron"

	\makescore 
  \relative do' {
	do fa te, me le  \bar "|"  ra, se de, fe taw  \bar "|"  re, sol do, fa te  \bar "|"  me, le ra, se de  \bar "|" \break  mi, la re, sol do  \bar "|"  fa, te me, le ra  \bar "|"  se, de fe, taw maw  \bar "|"  sol, do fa, te me  \bar "|" \break  le, ra se, de fe  \bar "|"  la, re sol, do fa  \bar "|"  te, me le, ra se  \bar "|"  ti, mi la, re sol  \bar "|" \break  do1
  }
  #'(( festival-tempo . 150 )( festival-voice . voice_us2_mbrola )( do-compile . #t )( do-play . #f ))

\paper  {
	#(set! paper-alist (cons '("a4insides" . (cons (* 8 in) (* 3 in))) paper-alist))
	#(set-paper-size "a4insides")
}

