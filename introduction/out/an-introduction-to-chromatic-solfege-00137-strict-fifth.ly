\version "2.18.2"
\include "aaron.ly"
\include "chromatic-template.ly"
\include "lilypond-book-preamble.ly"
\language "aaron"

	\makescore 
  \relative do' {
	\absolute { do' sol' re'' la'' mi'' ti'' fi''' di' si' ri'' li'' ma'' ta'' fai''' dai' sai' rai'' lai'' mai'' tai'' fao''' dao' sao' rao'' lao'' mao'' tao'' la''' }
  }
  #'(( festival-tempo . 100 )( festival-voice . voice_us2_mbrola )( do-compile . #t )( do-play . #f ))

\paper  {
	#(set! paper-alist (cons '("a4insides" . (cons (* 8 in) (* 3 in))) paper-alist))
	#(set-paper-size "a4insides")
}

