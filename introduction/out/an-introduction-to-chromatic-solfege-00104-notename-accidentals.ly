\version "2.18.2"
\include "aaron.ly"
\include "chromatic-template.ly"
\include "lilypond-book-preamble.ly"
\language "aaron"

	\makescore 
  \relative do' {
	do re mi fa sol la ti do  \bar "|"  do ti la sol fa mi re do  \bar "|" \break  di ri ma fi si li ta di  \bar "|"  di ta li si fi ma ri di  \bar "|" \break  dai rai mai fai sai lai tai dai  \bar "|"  dai tai lai sai fai mai rai dai  \bar "|" \break  dao rao mao fao sao lao tao dao  \bar "|"  dao tao lao sao fao mao rao dao  \bar "|" \break  dai rai mai fai sai lai tai dai  \bar "|"  dai tai lai sai fai mai rai dai  \bar "|" \break  di ri ma fi si li ta di  \bar "|"  di ta li si fi ma ri di  \bar "|" \break  do re mi fa sol la ti do  \bar "|"  do ti la sol fa mi re do  \bar "|" \break  de ra me fe se le te de  \bar "|"  de te le se fe me ra de  \bar "|" \break  daw raw maw faw saw law taw daw  \bar "|"  daw taw law saw faw maw raw daw  \bar "|" \break  dae rae mae fae sae lae tae dae  \bar "|"  dae tae lae sae fae mae rae dae  \bar "|" \break  daw raw maw faw saw law taw daw  \bar "|"  daw taw law saw faw maw raw daw  \bar "|" \break  de ra me fe se le te de  \bar "|"  de te le se fe me ra de  \bar "|" \break  do re mi fa sol la ti do  \bar "|"  do ti la sol fa mi re do  \bar "|" \break 
  }
  #'(( festival-tempo . 150 )( festival-voice . voice_us2_mbrola )( do-compile . #t )( do-play . #f ))

\paper  {
	#(set! paper-alist (cons '("a4insides" . (cons (* 8 in) (* 3 in))) paper-alist))
	#(set-paper-size "a4insides")
}

